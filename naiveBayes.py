# naiveBayes.py
# -------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

import util
import classificationMethod
import math

class NaiveBayesClassifier(classificationMethod.ClassificationMethod):
  """
  See the project description for the specifications of the Naive Bayes classifier.
  
  Note that the variable 'datum' in this code refers to a counter of features
  (not to a raw samples.Datum).
  """
  def __init__(self, legalLabels):
    self.legalLabels = legalLabels
    self.type = "naivebayes"
    self.k = 1 # this is the smoothing parameter, ** use it in your train method **
    self.automaticTuning = False # Look at this flag to decide whether to choose k automatically ** use this in your train method **
    
  def setSmoothing(self, k):
    """
    This is used by the main method to change the smoothing parameter before training.
    Do not modify this method.
    """
    self.k = k

  def train(self, trainingData, trainingLabels, validationData, validationLabels):
    """
    Outside shell to call your method. Do not modify this method.
    """  
      
    # might be useful in your code later...
    # this is a list of all features in the training set.
    self.features = list(set([ f for datum in trainingData for f in datum.keys() ]))

    if (self.automaticTuning):
        kgrid = [0.001, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 20, 50]
    else:
        kgrid = [self.k]
        
    self.trainAndTune(trainingData, trainingLabels, validationData, validationLabels, kgrid)

  def _initialize_mle_dict(self):
      mle_dict = {}
      for label in self.legalLabels:
          mle_dict[label] = {}
          for feature in self.features:
              mle_dict[label][feature] = {1: 0, 0: 0}

      return mle_dict

  def trainAndTune(self, trainingData, trainingLabels, validationData, validationLabels, kgrid):
    """
    Trains the classifier by collecting counts over the training data, and
    stores the Laplace smoothed estimates so that they can be used to classify.
    Evaluate each value of k in kgrid to choose the smoothing parameter 
    that gives the best accuracy on the held-out validationData.
    
    trainingData and validationData are lists of feature Counters.  The corresponding
    label lists contain the correct label for each datum.
    
    To get the list of all possible features or labels, use self.features and 
    self.legalLabels.
    """

    #print(trainingData[0])
    #print(len(trainingLabels))

    # keeps track of data indices belonging to the labels
    label_indices_map = {}
    for i, t_label in enumerate(trainingLabels):
        if t_label in self.legalLabels:
            if t_label not in label_indices_map.keys():
                label_indices_map[t_label] = []
            label_indices_map[t_label].append(i)

    # for every label keep track of count of features' values 1 or 0
    self.prob = {}
    mle_c = self._initialize_mle_dict()
    for label, indices in label_indices_map.items():
        self.prob[label] = (len(indices)*1.0)/len(trainingLabels)
        for data_index in indices:
            for feature in self.features:
                if trainingData[data_index][feature] == 1:
                    mle_c[label][feature][1] += 1
                else:
                    mle_c[label][feature][0] += 1

    # find the appropriate smoothing parameter using validation data
    smooth_mles = {}
    max_val = 0
    max_k = kgrid[0]
    for k in kgrid:
        smooth_mles[k] = self._initialize_mle_dict()
        for label, features_vals_cnt in mle_c.items():
            for feature, vals_cnt in features_vals_cnt.items():
                total_cnt = sum(vals_cnt.values()) + 2*k
                for val, cnt in vals_cnt.items():
                    smooth_mles[k][label][feature][val] = ((cnt+k)*1.0)/total_cnt

        correct_cnt = 0
        wrong_cnt = 0
        self.mle = smooth_mles[k]
        prediction_labels = self.classify(validationData)
        for i, original_label in enumerate(validationLabels):
            if prediction_labels[i] == original_label:
                correct_cnt += 1
            else:
                wrong_cnt += 1
        if (correct_cnt - wrong_cnt) > max_val:
            max_val = correct_cnt - wrong_cnt
            max_k = k

    print("Chosen k: {}".format(max_k))
    self.mle = smooth_mles[max_k]


  def classify(self, testData):
    """
    Classify the data based on the posterior distribution over labels.
    
    You shouldn't modify this method.
    """
    guesses = []
    self.posteriors = [] # Log posteriors are stored for later data analysis (autograder).
    for datum in testData:
      posterior = self.calculateLogJointProbabilities(datum)
      guesses.append(posterior.argMax())
      self.posteriors.append(posterior)
    return guesses
      
  def calculateLogJointProbabilities(self, datum):
    """
    Returns the log-joint distribution over legal labels and the datum.
    Each log-probability should be stored in the log-joint counter, e.g.    
    logJoint[3] = <Estimate of log( P(Label = 3, datum) )>
    
    To get the list of all possible features or labels, use self.features and 
    self.legalLabels.
    """
    logJoint = util.Counter()
    
    for label in self.legalLabels:
        logJoint[label] = math.log(self.prob[label])
        for feature in self.features:
            if datum[feature] == 1:
                logJoint[label] += math.log(self.mle[label][feature][1])
            else:
                logJoint[label] += math.log(self.mle[label][feature][0])

    return logJoint
  
  def findHighOddsFeatures(self, label1, label2):
    """
    Returns the 100 best features for the odds ratio:
            P(feature=1 | label1)/P(feature=1 | label2) 
    
    Note: you may find 'self.features' a useful way to loop through all possible features
    """
    featuresOdds = []
       
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

    return featuresOdds
    

    
      
